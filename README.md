# RSVP Calendar

A way of converting a [Microformats2](https://microformats.io) feed of [RSVPs](https://indieweb.org/rsvp) into an iCalendar feed, so you can track where you're going and what you're attending more easily.

## Use

Assuming that your feed that the RSVPs are present on is `https://www.jvt.me/kind/rsvps/`, you can point your browser / calendar application at `https://rsvp-calendar.tanna.dev/feed?url=https://www.jvt.me/kind/rsvps/`. On the first call, it will start to retrieve the RSVPs in the background, and on subsequent calls it will start to display events it has processed.
