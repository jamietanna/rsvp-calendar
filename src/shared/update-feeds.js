const arc = require('@architect/functions')
const logger = require('./logger')

module.exports = async function updateFeeds () {
  const data = await arc.tables()
  const feedUrls = await data.feeds.scan()
  await Promise.all(feedUrls.Items.map(async (item) => {
    logger.info(`Updating feed for ${item.url}`)
    await arc.events.publish({
      name: 'update-feed',
      payload: {
        url: item.url
      }
    })
  }))
  await logger.info(`Finished publishing ${feedUrls.Items.length} feed(s) for update`)
}
