const arc = require('@architect/functions')
const crypto = require('crypto')

async function log (type, message) {
  const data = await arc.tables()
  const published = new Date()
  const expires = Math.floor((new Date().setDate(new Date().getDate() + 7)) / 1000)
  const log = {
    log: 'log',
    id: crypto.randomBytes(8).toString('hex'),
    type: type.toLowerCase(),
    published: published.toISOString(),
    message,
    expires
  }
  if (process.env.ARC_ROLE && process.env.ARC_ROLE === 'SandboxRole') {
    console.log(JSON.stringify(log))
  }
  await data.logs.put(log)
}

async function info (message) {
  await log('info', message)
}

async function warn (message) {
  await log('warn', message)
}

async function error (message) {
  await log('error', message)
}

async function debug (message) {
  await log('debug', message)
}

module.exports = {
  info,
  warn,
  error,
  debug
}
