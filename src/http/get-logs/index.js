const arc = require('@architect/functions')
const hljs = require('highlight.js/lib/common')
const nunjucks = require('nunjucks')

const env = nunjucks.configure('views')
env.addFilter('messageTitle', function (message) {
  if (typeof message === 'string') {
    return message
  } else if (message.message) {
    return message.message
  } else {
    return `object: [${Object.keys(message).join(', ')}]`
  }
})
env.addFilter('highlight', function (str) {
  return hljs.highlightAuto(str).value
})

async function getLogs () {
  const data = await arc.tables()
  const opts = {
    IndexName: 'log-published-index',
    ScanIndexForward: false,
    KeyConditionExpression: '#log = :log',
    ExpressionAttributeNames: {
      '#log': 'log'
    },
    ExpressionAttributeValues: {
      ':log': 'log'
    },
    Limit: 100
  }
  const logs = await data.logs.query(opts)
  return logs.Items
}

exports.handler = async function http (req) {
  const logs = await getLogs()
  const body = nunjucks.render('index.njk', { logs })
  return {
    statusCode: 200,
    headers: {
      'Cache-Control': 'no-cache, no-store, must-revalidate, max-age=0, s-maxage=0',
      'Content-Type': 'text/html; charset=utf-8'
    },
    body
  }
}
