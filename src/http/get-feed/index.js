const arc = require('@architect/functions')
const logger = require('@architect/shared/logger')
const ical = require('ical-generator')
const updateFeeds = require('@architect/shared/update-feeds')
const htmlResponse = require('./html')

const notUndefined = anyValue => typeof anyValue !== 'undefined' // https://stackoverflow.com/a/51589677/2257038

async function createCalendar (url, events) {
  const cal = ical({ name: `RSVPs from ${url}` })
  await Promise.all(events.map(async function (e) {
    if (e.rsvp.rsvp === 'no') {
      logger.info(`Skipping RSVP (\`${e.rsvp.rsvp}\`) to ${e.rsvp.eventUrl}`)
      return
    }

    // ignore events without any context
    try {
      const event = await toEvent(e)
      cal.createEvent(event)
    } catch (_e) {
      logger.error({
        message: `Error for RSVP from ${e.rsvp.eventUrl} to ${e.event.eventUrl}`,
        error: _e
      })
    }
  }))
  logger.info(`Calendar for ${url} generated with ${cal.events().length} events`)
  return cal.toString()
}

async function addLocation (event, properties) {
  if (typeof properties.location[0] === 'string') {
    event.location = properties.location[0]
  } else {
    const loc = properties.location[0]
    const parts = []
    if (loc.type[0] === 'h-card') {
      parts.push(loc.properties.name[0])
    }

    const propertiesToAdd = [
      'street-address', 'locality', 'country-name'
    ]
    propertiesToAdd.forEach((p) => {
      if (loc.properties[p]) {
        parts.push(loc.properties[p][0])
      }
    })
    event.location = parts.join(', ')
  }
}

async function addDescription (description, property, properties) {
  if (typeof properties[property][0] === 'string') {
    description.html += `\n\n${properties[property][0]}`
    description.plain += `\n\n${properties[property][0]}`
  } else {
    description.html += `\n\n${properties[property][0].html}`
    description.plain += `\n\n${properties[property][0].value}`
  }
}

async function toEvent (p) {
  const e = p.event
  const properties = e.eventObject

  let summary
  if (e.rsvp !== 'yes') {
    summary = `[${e.rsvp}] `
  }

  if ('name' in properties) {
    summary = properties.name[0]
  } else {
    summary = `RSVP to ${properties.url}`
  }
  const url = (properties.url && properties.url[0]) || e.eventUrl

  const event = {
    summary: summary,
    stamp: new Date(p.rsvp.updated),
    start: new Date(properties.start[0]),
    end: new Date(properties.end[0]),
    url: url
  }

  const description = {
    html: `<p><em>More details can be found at <a href="${url}">${url}</a></em></p>.<br><br>`,
    plain: `More details can be found at ${url}.\n\n`
  }

  if ('content' in properties) {
    addDescription(description, 'content', properties)
  } else if ('description' in properties) {
    addDescription(description, 'description', properties)
  } else if ('summary' in properties) {
    addDescription(description, 'summary', properties)
  }

  description.html = description.html.replace(/(?:\r\n|\r|\n)/g, '<br>')

  event.description = description

  if ('location' in properties) {
    await addLocation(event, properties)
  }

  return event
}

async function calendar (url) {
  const data = await arc.tables()

  const rsvps = await data.rsvps.scan({
    ScanIndexForward: false,
    TableName: 'rsvps',
    FilterExpression: 'feedUrl = :feedUrl',
    ExpressionAttributeValues: { ':feedUrl': url }
  })

  logger.info(`Found ${rsvps.Items.length} items for ${url}`)

  const events = await Promise.all(rsvps.Items.map(async function (rsvp) {
    const event = await data.events.get({
      eventUrl: rsvp.eventUrl
    })

    if (event === undefined || event.eventObject === undefined) {
      logger.info(`Skipping rendering ${rsvp.eventUrl} as it could not be found`)
      return
    }
    return {
      rsvp: rsvp,
      event: event
    }
  })).then((e) => e.filter(notUndefined))

  const cal = await createCalendar(url, events)
  return cal.toString()
}

async function addFeedUrl (url) {
  const data = await arc.tables()
  data.feeds.put({ url })
}

async function maybeUpdateFeeds () {
  if (process.env.ARC_ROLE && process.env.ARC_ROLE === 'SandboxRole') {
    updateFeeds()
  }
}

exports.handler = async function http (req) {
  const url = req.queryStringParameters.url
  addFeedUrl(url)

  maybeUpdateFeeds()

  logger.info({ message: `Generating calendar for ${url}`, userAgent: `${JSON.stringify(req.headers['user-agent'])}` })
  const ics = await calendar(url)

  if (await htmlResponse.isRequired(req)) {
    return await htmlResponse.render(url, ics)
  }
  return {
    statusCode: 200,
    headers: {
      'cache-control': 'no-cache, no-store, must-revalidate, max-age=0, s-maxage=0',
      'content-type': 'text/calendar; charset=utf8'
    },
    body: ics
  }
}
