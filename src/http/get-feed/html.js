const logger = require('@architect/shared/logger')
const accept = require('@hapi/accept')
const nunjucks = require('nunjucks')
nunjucks.configure('views')

async function isRequired (req) {
  const userAgent = req.headers['user-agent']
  logger.info({
    message: 'Determining whether request requires HTML response',
    userAgent: JSON.stringify(userAgent),
    acceptHeader: `${JSON.stringify(req.headers.accept)}`
  })
  // no accept header is sent, so we can make sure that we force their response
  if (userAgent === 'Google-Calendar-Importer') {
    return false
  }
  const html = 'text/html'
  const accepted = accept.mediaTypes(req.headers.accept, [html])
  const required = accepted.includes(html)
  await logger.info({
    message: `Request does${required ? '' : ' not'} require HTML resposne`,
    userAgent: JSON.stringify(userAgent),
    acceptHeader: `${JSON.stringify(req.headers.accept)}`
  })
  return required
}

async function render (url, ics) {
  const body = nunjucks.render('index.njk', { url, ics })

  return {
    headers: {
      'Content-Type': 'text/html; charset=utf8',
      'Referrer-Policy': 'no-referrer',
      'X-Frame-Options': 'DENY'
    },
    statusCode: 200,
    body: body
  }
}

module.exports = {
  isRequired,
  render
}
