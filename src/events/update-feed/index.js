const arc = require('@architect/functions')
const logger = require('@architect/shared/logger')
const { mf2 } = require('microformats-parser')
const fetch = require('node-fetch')

function eventUrl (inReplyTo) {
  if (typeof inReplyTo[0] === 'string') {
    return inReplyTo[0]
  } else {
    return inReplyTo[0].properties.url[0]
  }
}

const notUndefined = anyValue => typeof anyValue !== 'undefined' // https://stackoverflow.com/a/51589677/2257038

async function rsvpToRsvpEventPayload (url, r) {
  const rsvp = {
    feedUrl: url,
    rsvp: r.properties.rsvp[0],
    rsvpUrl: r.properties.url[0],
    eventUrl: eventUrl(r.properties['in-reply-to'])
  }

  if ('updated' in r.properties) {
    rsvp.updated = r.properties.updated[0]
  } else if ('published' in r.properties) {
    rsvp.updated = r.properties.published[0]
  } else {
    rsvp.updated = new Date()
  }
  return rsvp
}

async function findRsvps (parsed) {
  return parsed.items
    .filter((item) => {
      return item.type[0] === 'h-feed' || item.type[0] === 'h-entry'
    })
    .flatMap((item) => {
      if ('children' in item) {
        return item.children
          .filter((child) => {
            return ('rsvp' in child.properties)
          })
      } else if ('rsvp' in item.properties) {
        return item
      } else {
        return undefined
      }
    }).filter(notUndefined)
}

async function parseAndUpdateFeed ({ url }) {
  logger.info({ message: `Starting to update feed for ${url}`, url: url })
  const rsvps = await fetch(url)
    .then(res => res.text())
    .then(body => mf2(body, {
      baseUrl: url
    }))
    .then(parsed => findRsvps(parsed))

  logger.info({ message: `Found ${rsvps.length} RSVPs for URL ${url}`, url: url })

  await Promise.all(rsvps.map(async (r) => {
    await arc.events.publish({
      name: 'add-rsvp',
      payload: await rsvpToRsvpEventPayload(url, r)
    })
  }))
}

exports.handler = async function subscribe (event) {
  const body = JSON.parse(event.Records[0].Sns.Message)
  await parseAndUpdateFeed(body)
}
