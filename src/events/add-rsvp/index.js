const arc = require('@architect/functions')

exports.handler = async function subscribe (event) {
  const body = JSON.parse(event.Records[0].Sns.Message)
  const data = await arc.tables()
  await Promise.all([
    data.rsvps.put(body),
    arc.events.publish({
      name: 'fetch-event-context',
      payload: {
        eventUrl: body.eventUrl
      }
    })])
}
