const arc = require('@architect/functions')
const logger = require('@architect/shared/logger')
const eventbrite = require('./eventbrite')
const meetup = require('./meetup')
const mf2 = require('./mf2')

async function getHandler (url) {
  // for specific sites, use custom parsing
  if (meetup.isMeetupUrl(url)) {
    return meetup
  } else if (eventbrite.isEventbriteUrl(url)) {
    return eventbrite
  } else {
    return mf2
  }
}

async function getContext (handler, url) {
  const properties = await handler.fetchContext(url)
  if (properties) {
    return properties
  }
}

// Via https://stackoverflow.com/a/1353711/2257038
async function isDate (str) {
  const d = new Date(str)
  return d instanceof Date && !isNaN(d)
}

async function validateDate (eventObject, property, errors) {
  if (property in eventObject && eventObject[property].length !== 0) {
    const date = eventObject[property][0]
    if (!await isDate(date)) {
      errors.push(`eventObject did not contain a valid date in the \`${property}\` property`)
    }
  } else {
    errors.push(`eventObject did not contain a \`${property}\` property`)
  }
}

async function validateEventObject (eventObject) {
  const errors = []
  if (eventObject === undefined) {
    errors.push('The eventObject was `undefined`')
    return errors
  }

  await Promise.all([
    validateDate(eventObject, 'start', errors),
    validateDate(eventObject, 'end', errors)
  ])

  return errors
}

exports.handler = async function subscribe (event) {
  const body = JSON.parse(event.Records[0].Sns.Message)
  const data = await arc.tables()

  const found = await data.events.get({ eventUrl: body.eventUrl })
  const exists = found && found.eventObject

  if (exists) {
    logger.info({
      message: `Skipping fetch-event-context for ${body.eventUrl} as it's already been fetched before`,
      event: found.eventObject
    })
    return
  }

  const handler = await getHandler(body.eventUrl)

  logger.info({ message: `Processing fetch-event-context for ${body.eventUrl}`, handler: handler.name })

  const payload = {
    eventUrl: body.eventUrl,
    eventObject: await getContext(handler, body.eventUrl)
  }
  const errors = await validateEventObject(payload.eventObject)
  if (errors.length !== 0) {
    logger.warn({
      message: `Aborting fetch-event-context for ${body.eventUrl} as the eventObject has been deemed as invalid`,
      errors: errors
    })
    return
  }
  logger.info({
    message: `Finished processing fetch-event-context for ${body.eventUrl}`,
    event: payload.eventObject
  })

  await data.events.put(payload)
}
