const logger = require('@architect/shared/logger')
const fetch = require('node-fetch')

function name () {
  return 'Eventbrite'
}

function isEventbriteUrl (url) {
  return ((url.indexOf('https://eventbrite.com') > -1) ||
    (url.indexOf('https://www.eventbrite.com') > -1) ||
    (url.indexOf('https://eventbrite.co.uk') > -1) ||
    (url.indexOf('https://www.eventbrite.co.uk') > -1))
}

function getEventbriteUrl (url) {
  const parsed = new URL(url)
  return 'https://eventbrite-mf2.herokuapp.com' + parsed.pathname
}

async function fetchContext (url) {
  if (!isEventbriteUrl(url)) {
    return
  }
  const urlToFetch = getEventbriteUrl(url)
  const response = await fetch(urlToFetch)
  logger.info({
    message: 'Retrieving event context',
    handler: name,
    url,
    urlToFetch
  })
  if (!response.ok) {
    return
  }
  const mf2 = await response.json()
  if (!('items' in mf2) || !mf2.items.length || !mf2.items[0].properties) return

  return mf2.items[0].properties
}

module.exports = {
  name,
  isEventbriteUrl,
  fetchContext
}
