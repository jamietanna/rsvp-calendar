const logger = require('@architect/shared/logger')
const { mf2 } = require('microformats-parser')
const fetch = require('node-fetch')

function name () {
  return 'Microformats2'
}

async function fetchContext (url) {
  logger.info({
    message: 'Retrieving event context',
    handler: name,
    url,
    urlToFetch: url
  })

  const parsed = await fetch(url)
    .then(res => res.text())
    .then(body => mf2(body, {
      baseUrl: url
    }))

  if (!('items' in parsed) || !parsed.items.length || !parsed.items[0].properties) return

  const found = parsed.items
    .filter((item) =>
      item.type[0] === 'h-event'
    )

  if (!found) {
    return null
  }

  if (found.length !== 1) {
    logger.error(`Note that multiple events (${found.length}) were found when parsing ${url}`)
  }

  return found[0].properties
}

module.exports = {
  name,
  fetchContext
}
