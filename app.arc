@app
rsvp-calendar

@http
get /
get /feed
get /logs

@tables
rsvps
  feedUrl *String
  rsvpUrl *String
  rsvp String
  eventUrl String
  updated **String

events
  eventUrl *String
  eventObject String

logs
  id *String
  expires TTL

feeds
  url *String

@tables-indexes
logs
  log *String
  published **String

@events
update-feed
fetch-event-context
add-rsvp

@scheduled
update-feeds rate(1 hour)

@aws
profile default
region eu-west-2
timeout 30
architecture arm64
